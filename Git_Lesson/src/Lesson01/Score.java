package Lesson01;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Score {
	public static void main(String[] args) throws FileNotFoundException,IOException {
		//读取两个网页
		File f1 = new File("src/small.html");
		File f2 = new File("src/all.html");
		Document d1 = Jsoup.parse(f1, "UTF-8");
		Document d2 = Jsoup.parse(f2, "UTF-8");
		//读取配置文件
		Properties p1 = new Properties();
		p1.load(new FileInputStream("src/total.properties"));
		//初始化数据
		int myBefore = 0;
		int myProgram = 0;
		int myAdd = 0;
		int myTest = 0;
		int myBase = 0;
		double fullBefore = Double.parseDouble(p1.getProperty("before"));
		double fullProgram = Double.parseDouble(p1.getProperty("program"));
		double fullAdd = Double.parseDouble(p1.getProperty("add"));
		double fullTest = Double.parseDouble(p1.getProperty("test"));
		double fullBase = Double.parseDouble(p1.getProperty("base"));
		//经验统计
		Elements e1 = d1.getElementsByClass("interaction-row");
		Elements e2 = d2.getElementsByClass("interaction-row");
		Scanner sc = null;
		for (int i = 0; i < e1.size(); i++) {
			if (e1.get(i).child(1).child(0).toString().contains("课前自测") && e1.get(i).child(1).child(2).toString().contains("color:#8FC31F")) {
				sc = new Scanner(e1.get(i).child(1).child(2).children().get(0).children().get(10).text());
				myBefore += sc.nextInt();
			} else if (e1.get(i).child(1).child(0).toString().contains("编程题") && e1.get(i).child(1).child(2).toString().contains("已参与")) {
				sc = new Scanner(e1.get(i).child(1).child(2).children().get(0).children().get(7).text());
				myProgram += sc.nextInt();
			}else if (e1.get(i).child(1).child(0).toString().contains("附加题") && e1.get(i).child(1).child(2).toString().contains("已参与")) {
				sc = new Scanner(e1.get(i).child(1).child(2).children().get(0).children().get(10).text());
				myAdd += sc.nextInt();
			}else if (e1.get(i).child(1).child(0).toString().contains("课堂小测") && e1.get(i).child(1).child(2).toString().contains("已参与")) {
				sc = new Scanner(e1.get(i).child(1).child(2).children().get(0).children().get(7).text());
				myTest += sc.nextInt();
			}else if (e1.get(i).child(1).child(0).toString().contains("课堂完成") && e1.get(i).child(1).child(2).toString().contains("已参与")) {
				sc = new Scanner(e1.get(i).child(1).child(2).children().get(0).children().get(7).text());
				myBase += sc.nextInt();
			}
		}
		
		for (int i = 0; i < e2.size(); i++) {
			if (e2.get(i).child(1).child(0).toString().contains("附加题") && e2.get(i).child(1).child(2).toString().contains("已参与")) {
				sc = new Scanner(e2.get(i).child(1).child(2).children().get(0).children().get(10).text());
				myAdd += sc.nextInt();
			}
		}
		//根据比例算出最终成绩并输出
		double lastBefore = myBefore / fullBefore * 100 * 0.25;
		double lastProgram = myProgram / fullProgram * 100;
		if (lastProgram >= 95.00) {
			lastProgram = 95.00 * 0.1;
		}else {
			lastProgram = lastProgram * 0.1;
		}
		double lastAdd = myAdd / fullAdd * 100;
		if (lastAdd >= 90.00) {
			lastAdd = 90.00 * 0.05;
		}else {
			lastAdd = lastAdd * 0.05;
		}
		double lastTest = myTest / fullTest * 100 * 0.2;
		double lastBase = myBase / fullBase * 100 * 0.3 * 0.95;

		System.out.println(lastBefore+lastProgram+lastAdd+lastTest+lastBase+6);
	}
}